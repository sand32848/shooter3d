using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	[SerializeField] private float damageMulti = 1;
	private float damage = 1;

	private void OnTriggerEnter(Collider other)
	{
		if (other.transform.CompareTag("Wall"))
		{
			Destroy(gameObject);
		}

		if (!other.CompareTag("Enemy"))
		{
			return;
		}

		if (other.transform.parent.TryGetComponent(out Health enemy))
		{
			enemy.reduceHealth(damage);
			Destroy(gameObject);
		}
	}

	public void setDamage(float _damage)
	{
		damage = _damage * damageMulti;
	}
}
