using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public abstract class GunUpgrade : MonoBehaviour
{
	public GunStat gunStat;
	public int maxLevel;
	public int currentLevel;
	[SerializeField] public List<upgradeStat> upgradeStat = new List<upgradeStat>();

	private void Start()
	{
		gunStat = GetComponent<GunStat>();
		maxLevel = upgradeStat.Count;
	}

	public abstract void upgradeTrigger();

	public virtual float applyUpgrade(float _gunstat =  0)
	{
		if(currentLevel >= maxLevel)
		{
			return upgradeStat[upgradeStat.Count -1 ].statLevel;
		}

		currentLevel++;

		return upgradeStat[currentLevel -1].statLevel;
	}

	public float ReturnCurrentCost()
    {
		int i = currentLevel -1;
		i = Mathf.Clamp(i,0, maxLevel - 1);
		return upgradeStat[i].costLevel;
    }
}

[System.Serializable]
public class upgradeStat{
	[SerializeField] public int costLevel;
	[SerializeField] public float statLevel;
}
