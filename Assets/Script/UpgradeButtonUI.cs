using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class UpgradeButtonUI : MonoBehaviour
{
    [SerializeField] private GunUpgrade gunUpgrade;
    [SerializeField] protected TextMeshProUGUI upgradeCostText;
    [SerializeField] protected TextMeshProUGUI upgradeLevelText;
	private Button upgradeButton;
	public static Action<float> onUpgradePress;

    private void Awake()
    {
        upgradeButton = GetComponent<Button>();
    }
    private void Start()
	{
		updateUpgradeUI();

	}

    private void Update()
    {
        if(gunUpgrade.currentLevel == gunUpgrade.maxLevel)
        {
            upgradeButton.interactable = false;
            return;
        }

        if (MoneyManager.Instance.moneyBank < gunUpgrade.ReturnCurrentCost())
        {
            upgradeButton.interactable = false;
        }
        else 
        {
            upgradeButton.interactable = true;
        }
    }

    public void updateUpgradeUI()
	{        
        if (gunUpgrade.currentLevel == gunUpgrade.maxLevel)
        {
            upgradeButton.interactable = false;
            upgradeLevelText.text = "Lvl: Max";
            upgradeCostText.text = $"Cost: Max";

            return;
        }


        if (upgradeLevelText) {

            if (gunUpgrade.currentLevel != gunUpgrade.maxLevel)
            {
                upgradeLevelText.text = $"Lvl: {gunUpgrade.currentLevel.ToString()}";
            }

        } 

		if (upgradeCostText) 
		{
			if (gunUpgrade.currentLevel != gunUpgrade.maxLevel )
			{
				upgradeCostText.text = $"Cost: {gunUpgrade.upgradeStat[gunUpgrade.currentLevel].costLevel.ToString()}";
			}
  
		} 
    }

    public void paidMoney()
    {
        onUpgradePress?.Invoke(-gunUpgrade.ReturnCurrentCost());
    }


}
