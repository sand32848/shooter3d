using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseController : MonoBehaviour
{
	[SerializeField] private List<float> phaseHealth = new List<float>();
	private Health healthStat;
	public int phaseIndex { get; private set; } = 0;

	private void Start()
	{
		healthStat = GetComponent<Health>();
	}

	public void updatePhase()
	{
		for (int i = 0; i < phaseHealth.Count; i++)
		{
			if(healthStat.getHealthPercent() <= phaseHealth[i])
			{
				phaseIndex = i + 1;
				return;
			}
		}
		healthStat.getHealthPercent() ;
	}
}
