using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class ReloadUI : MonoBehaviour
{

	[SerializeField] TextMeshProUGUI ammoCount;
	[SerializeField] Image slider;
	private void OnEnable()
	{
		GunStat.updateAmmoCount += updateAmmoUI;
		GunReload.gunReloadStat += Reloading;
	}

	private void OnDisable()
	{
		GunStat.updateAmmoCount -= updateAmmoUI;
		GunReload.gunReloadStat -= Reloading;
;
	}

	public void Reloading(float reloadTime, float maxAmmo)
    {
		StartCoroutine(reloading(reloadTime, maxAmmo));
    }

	IEnumerator reloading(float reloadTime, float maxAmmo)
    {
		ammoCount.text = "Reloading";
		DOTween.To(() => slider.fillAmount, x =>  slider.fillAmount = x, 1, reloadTime);
		yield return new WaitForSeconds(reloadTime);
		updateAmmoUI((int)maxAmmo,(int)maxAmmo);
    }

	public void updateAmmoUI(int currentAmmo =  0, int maxAmmo = 0)
	{
		ammoCount.text = currentAmmo.ToString() + " / " + maxAmmo.ToString();
		slider.fillAmount = ((float)currentAmmo / (float)maxAmmo);
	}
}
