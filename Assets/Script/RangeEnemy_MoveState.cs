using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy_MoveState : EnemyState
{
	[SerializeField] private float speed;
	[SerializeField] private float enterRange;
	public override void RunCurrentState()
	{
		navMesh.speed = speed;

		if (enemyAI.distanceBetweenPlayer() < enterRange)
		{
			stateManager.runState(typeof(RangeEnemy_ShootState));
		}
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere(transform.position,enterRange);
	}
}
