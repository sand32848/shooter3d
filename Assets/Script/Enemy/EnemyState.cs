using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class EnemyState : State
{
	protected EnemyAI enemyAI;

	protected NavMeshAgent navMesh;
	protected StateManager stateManager;
	protected PhaseController phaseController;
	
	private void OnEnable()
	{
		stateManager = transform.parent.GetComponent<StateManager>();
		navMesh = transform.parent.GetComponent<NavMeshAgent>();
		enemyAI = transform.parent.GetComponent<EnemyAI>();

		phaseController = transform.parent.GetComponent<PhaseController>();
	}


}
