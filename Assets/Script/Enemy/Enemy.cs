using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Enemy : MonoBehaviour
{
	public static Action onDeath;
  
    public  void DeathSignal()
	{
		onDeath?.Invoke();
	}
}
