using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;
public class EnemyAI : MonoBehaviour
{
    protected bool active = false;

    public GameObject player { get; private set; }
    public Animator animator { get; private set; }
    public Health health { get; set; }
    

    private void Start()
	{
        health = GetComponent<Health>();
    }

	public void OnEnable()
	{
        LoadComponent();
	}

    public virtual void setActive()
	{
        active = true;
	}

	public void LoadComponent()
	{
        player = GameObject.FindWithTag("Player");
        animator = GetComponent<Animator>();
    }

    public Vector3 DirectionToPlayer()
	{
        Vector3 d = player.transform.position - transform.position;
        return d.normalized;
    }

    public float distanceBetweenPlayer()
	{
        float distance = Vector3.Distance(transform.position, player.transform.position);
        return distance; 

    }

    public void playDeathSound()
    {
        AudioManager.instance?.Play("EnemyDead");
    }
}

