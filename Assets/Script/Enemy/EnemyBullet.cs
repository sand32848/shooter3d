using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
	[SerializeField] private float damageMulti = 1;
	private float damage = 1;

	private void OnTriggerEnter(Collider other)
	{
		
		if (other.transform.CompareTag("Wall"))
		{
			Destroy(gameObject);
		}

		if (!other.transform.parent.CompareTag("Player"))
		{
			return;
		}

		if (other.transform.parent.TryGetComponent(out PlayerHealth player))
		{
			player.reduceHealth(damage);
			Destroy(gameObject);
		}
	}

	public void setDamage(float _damage)
	{
		damage = _damage * damageMulti;
	}
}
