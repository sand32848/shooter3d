using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotGun : MonoBehaviour
{
	[SerializeField] public int shotAmount;
	[SerializeField] private float spread;
	private GunShoot gunShoot;

	private void Start()
	{
		gunShoot = GetComponent<GunShoot>();
	}

	public void shotGunShoot()
	{
		gunShoot.castRay(spread,shotAmount);
	}

	
}
