using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAnimator : MonoBehaviour
{
    private Animator gunAnimator;

    void Start()
    {
        gunAnimator = GetComponent<Animator>();
    }

    public void playerGunAnimation(string animationName)
	{
        gunAnimator.Play(animationName);
	}
}
