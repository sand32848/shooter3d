using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGunMultiplier : MonoBehaviour
{
    public float fireRateMulti { get; set; } = 1;
    public float bulletSpeedMulti { get; set; } = 1;
    public float damageMulti { get; set; } = 1;
    public float reloadMulti { get; set; } = 1;

    public void ResetShootStat()
	{
        damageMulti = 1;
        fireRateMulti = 1;
        reloadMulti = 1;
        bulletSpeedMulti = 20;
	}

    public void ChangeShootDamage(float newDamage)
	{
        damageMulti = newDamage;
	}

    public void ChangeFireRate(float newFirerate)
	{
        fireRateMulti = newFirerate;
	}

    public void ChangeProjectileSpeed(float newSpeed)
	{
        bulletSpeedMulti = newSpeed;
	}
}
