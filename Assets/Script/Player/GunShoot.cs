using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using Cinemachine;

public class GunShoot : MonoBehaviour
{
    [SerializeField] private LayerMask toHit;
    [SerializeField] private UnityEvent onShootEvent;
    [SerializeField] private LineFolder lineFolder;
    [SerializeField] private GameObject gunBarrel;
    private bool shotCooldown = false;
    private GunStat gunStat;
    private GunReload gunReload;
    private GunAnimator gunAnimator;
	// Start is called before the first frame update

	private void Start()
	{
        gunStat = GetComponent<GunStat>();
        gunReload = GetComponent<GunReload>();
        gunAnimator = GetComponent<GunAnimator>();
	}

	void Update()
    {
        if (gunStat.shootMode == ShootMode.single)
        {
            SingleFire();
        }

        if (gunStat.shootMode == ShootMode.auto)
        {
            AutoFire();
        }
    }

    private void OnEnable()
    {
        shotCooldown = false;
    }

    private void Shooting()
    {
        if (gunReload.isReloading)
        {
            return;
        }

        if (shotCooldown)
		{
            return;
		}

        if (gunStat.currentAmmo <= 0)
        {
            AudioManager.instance?.Play("AmmoOut");
            return;
        }

        onShootEvent.Invoke();

        StartCoroutine(SetShotCooldown());

        gunStat.currentAmmo -= 1;

        AudioManager.instance?.Play("Gun");

        gunStat.CallUpdateAmmo();
    }
    IEnumerator SetShotCooldown()
    {
        shotCooldown = true;
        yield return new WaitForSeconds(gunStat.fireRate);
        shotCooldown = false;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator DrawGunLine(Vector3 dir, int lineAmount = 1)
	{
        for(int i = 0; i < lineAmount; i++)
		{

            Ray ray = Camera.main.ScreenPointToRay(InputController.Instance.mouseInput);
            RaycastHit rayInfo;

            if (Physics.Raycast(ray, out rayInfo, 30000))
            {
                RaycastHit r;

                Vector3 dir2 = (new Vector3(rayInfo.point.x, transform.position.y, rayInfo.point.z) - transform.position).normalized;

                if (Physics.Raycast(transform.position, dir, out r, 888888, toHit))
                {
                    if (lineFolder.lineList[i].enabled == false)
                    {
                        lineFolder.lineList[i].SetPosition(1, transform.InverseTransformPoint(r.point));
                        lineFolder.lineList[i].enabled = true;
                        yield return new WaitForSeconds(0.05f);
                        lineFolder.lineList[i].enabled = false;
                    }
                }
            }
        }
    }

    private void SingleFire()
    {
        if (InputController.Instance.LeftClick)
        {
            Shooting();
        }
    }

    private void AutoFire()
    {
        if (InputController.Instance.LeftClickHold)
        {
            Shooting();
        }
    }

    public void castRay()
    {
        Ray ray = Camera.main.ScreenPointToRay(InputController.Instance.mouseInput);
        RaycastHit rayInfo;

        if (Physics.Raycast(ray, out rayInfo, 30000))
        {
            RaycastHit r;

            Vector3 dir = (new Vector3(rayInfo.point.x, transform.position.y, rayInfo.point.z) - transform.position).normalized;

            StartCoroutine(DrawGunLine(dir));

            if (Physics.Raycast(transform.position, dir, out r, 888888, toHit))
            {
                if (r.collider.TryGetComponent(out HitBox hitbox))
                {
                    hitbox.hit(gunStat.firePower);
                }
            }
        }

   
    }

    public void castRay(float deviatedValue, int shotAmount = 1)
    {
        Ray ray = Camera.main.ScreenPointToRay(InputController.Instance.mouseInput);
        RaycastHit rayInfo;

        if (Physics.Raycast(ray, out rayInfo, 300))
        {
            RaycastHit r;

            for (int i = 0; i < shotAmount; i++)
			{
                Vector3 dir = (new Vector3(rayInfo.point.x, transform.position.y, rayInfo.point.z) - transform.position).normalized;

                var d = UnityEngine.Random.Range(-deviatedValue, deviatedValue);
                dir = Quaternion.Euler(0, d, 0) * dir;

                StartCoroutine(DrawGunLine(dir, shotAmount));

                if (Physics.Raycast(transform.position, dir, out r, 888888, toHit))
                {
                    if (r.collider.TryGetComponent(out HitBox hitbox))
                    {
                        hitbox.hit(gunStat.firePower);
                    }
                }
            }
    
        }
    }
}
