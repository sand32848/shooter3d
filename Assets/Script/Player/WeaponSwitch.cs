using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WeaponSwitch : MonoBehaviour
{
	private bool isReloading;
    [SerializeField] private List<GameObject> weaponList = new List<GameObject>();
	public static Action<int> onWeaponSwitch;

    private void OnEnable()
    {
		GunReload.gunReloadTrigger += toggleIsReload;
		GunReload.gunReloadFinish += toggleIsReload;
    }

    private void OnDisable()
    {
		GunReload.gunReloadTrigger -= toggleIsReload;
		GunReload.gunReloadFinish -= toggleIsReload;
	}

    private void Start()
	{
		ChangeWeapon(0);
	}

	void Update()
    {
		if (InputController.Instance.Weapon1)
		{
			ChangeWeapon(0);
		}
		if (InputController.Instance.Weapon2)
		{
			ChangeWeapon(1);
		}
		if (InputController.Instance.Weapon3)
		{
			ChangeWeapon(2);
		}
    }

	public void ChangeWeapon(int weaponIndex)
	{

		if (isReloading)
		{
			return;
		}

		onWeaponSwitch?.Invoke(weaponIndex);


		if (!weaponList[weaponIndex])
		{
			return;
		}

		if (weaponList[weaponIndex].activeSelf)
		{
			return;
		}

		for (int i = 0; i < weaponList.Count; i++)
		{
			weaponList[i].SetActive(false);
		}

		weaponList[weaponIndex].SetActive(true);

	}

	public void toggleIsReload()
    {
		isReloading = !isReloading;
    }
}
