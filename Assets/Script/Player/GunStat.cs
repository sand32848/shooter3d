using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GunStat : MonoBehaviour
{
    public static Action<int, int> updateAmmoCount { get; set; }

    [SerializeField] public float firePower = 5;
    [SerializeField] public float reloadTime = 2;
    [SerializeField] public float fireRate  = 2;
    [SerializeField] public float bulletSpeed;
    [SerializeField] public float MaxAmmo  = 10;
    [SerializeField] public ShootMode shootMode;
    [SerializeField] private bool unlock = false;
    [SerializeField] public int currentAmmo { get { return _currentAmmo; } set 
        {
            _currentAmmo = value;
            CallUpdateAmmo();
        } }
    [SerializeField] private int _currentAmmo;

    public GunAction currentGunAction = GunAction.isIdle;

	private void OnEnable()
	{

        CallUpdateAmmo();
	}

	public void CallUpdateAmmo()
	{
        updateAmmoCount?.Invoke(currentAmmo, (int)MaxAmmo);
	}

    public void UnlockGun()
	{
        unlock = true;
	}
}

public enum GunAction {isReloading, isShooting, isIdle,isAimming}
public enum ShootMode {single,auto }
