using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private State currentState;
    private CharacterController controller;
    private Animator animator;
    public float speed { get; set; } = 12f;

	private void Awake()
	{
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
      
    }

    private void FixedUpdate()
    {
        Vector3 move = new Vector3(InputController.Instance.movement.x,0, InputController.Instance.movement.y) ;

        controller.Move(move * speed *Time.deltaTime);

        if(move == Vector3.zero)
        {
            animator.Play("PlayerIdle");
        }
		else
		{
            animator.Play("PlayerRun");
        }
    }

    private void ResetMovementStat()
	{
        speed = 12;
	}

    public void ChangeMoveSpeed(float newSpeed)
	{
        speed = newSpeed;
	}
}
