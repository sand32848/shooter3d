using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using DG.Tweening;

public class GunZoom : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera headCam;
    [SerializeField] private float ZoomOutFov;
    [SerializeField] private float ZoomFov;
    private GunAnimator gunAnimator;
    public bool isAiming { get; private set; } = false;
	// Start is called before the first frame update

	private void Start()
	{
        gunAnimator = GetComponent<GunAnimator>();
	}
	// Update is called once per frame
	void Update()
    {
        ZoomIn();
    }

    public void ZoomIn()
	{
        if (InputController.Instance.RightClick)
		{
            isAiming = true;
            gunAnimator.playerGunAnimation("Aimming");
            DOTween.Kill("ZoomOut");
            DOTween.To(() => headCam.m_Lens.FieldOfView, x => headCam.m_Lens.FieldOfView = x, ZoomFov, 0.5f).SetId("ZoomIn");
        }
		else
        {
            if(isAiming == true)
            {
                gunAnimator.playerGunAnimation("DisAim");
                isAiming = false;
			}

            DOTween.Kill("ZoomIn");
            DOTween.To(() => headCam.m_Lens.FieldOfView, x => headCam.m_Lens.FieldOfView = x, 80, 0.5f).SetId("ZoomOut");
        }

    }
}
