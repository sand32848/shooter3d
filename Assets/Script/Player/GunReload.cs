using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GunReload : MonoBehaviour
{
    private GunStat gunStat;
    public static Action<float,float> gunReloadStat { get; set; }
    public static Action gunReloadTrigger { get; set; }
    public static Action gunReloadFinish { get; set; }

    public bool isReloading {   get; private set; }

    private float dummyReloadTime;
    private float _dummyReloadTime;

    private GunAnimator gunAnimator;
    // Start is called before the first frame update
    void Start()
    {
        gunStat = GetComponent<GunStat>();
        gunAnimator = GetComponent<GunAnimator>();
        dummyReloadTime = gunStat.reloadTime;
        _dummyReloadTime = dummyReloadTime;
    }

    // Update is called once per frame
    void Update()
    {
        reload();
    }

    private void reload()
	{
        if(gunStat.currentAmmo == gunStat.MaxAmmo)
		{
            return;
		}

        if (InputController.Instance.R && !isReloading)
        {
            //gunAnimator.playerGunAnimation("Reload");

            isReloading = true;
      
            gunReloadStat?.Invoke(gunStat.reloadTime,gunStat.MaxAmmo);
            gunReloadTrigger?.Invoke();
        }

		if (isReloading)
		{
            dummyReloadTime -= Time.deltaTime;

            if(dummyReloadTime <= 0)
			{
                reloadFinish();
			}
		}
    }

    private void reloadFinish()
    {
        isReloading = false;
        dummyReloadTime = _dummyReloadTime;
        gunStat.currentAmmo = (int)gunStat.MaxAmmo;
        gunReloadFinish?.Invoke();
    }
}
