using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    [SerializeField] private LayerMask groundLayer;
    private Vector3 lookAtPosition;
    // Start is called before the first frame update

    void Update()
    {
        Rotating();
        transform.rotation = Quaternion.LookRotation(lookAtPosition);
    }

	private void FixedUpdate()
	{

    }

	private void Rotating()
    {

        Ray ray = Camera.main.ScreenPointToRay(InputController.Instance.mouseInput);
        RaycastHit rayInfo;

        if (Physics.Raycast(ray, out rayInfo, 300,groundLayer))
        {
			if (InputController.Instance.LeftClick)
			{
                Debug.DrawRay(Camera.main.transform.position, rayInfo.point - Camera.main.transform.position, Color.green,50);
            }
    
            lookAtPosition = (new Vector3(rayInfo.point.x, transform.position.y, rayInfo.point.z) - transform.position).normalized;
            lookAtPosition.y = transform.position.y;
        }



        Ray ray2 = Camera.main.ViewportPointToRay(InputController.Instance.mouseInput);
        RaycastHit rayInfo2;
        if (Physics.Raycast(ray2, out rayInfo2, 300, groundLayer))
        {
            if (InputController.Instance.LeftClick)
            {
                Debug.DrawRay(Camera.main.transform.position, rayInfo.point - Camera.main.transform.position, Color.green, 50);
            }
        }
    }


}
