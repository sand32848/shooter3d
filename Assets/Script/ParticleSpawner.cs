using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    [SerializeField] private GameObject deathParticle;
    [SerializeField] private Color deathColor;
    [SerializeField] private MeshRenderer mesh;

    public void spawnParticle()
    {
        if (deathParticle)
        {
    
            GameObject p = Instantiate(deathParticle, transform.position, Quaternion.identity);

            var _main = p.GetComponent<ParticleSystem>().main;
             _main.startColor = mesh.material.color;
            

            Destroy(p, 2);
        }
    }
}
