using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunIcon : MonoBehaviour
{
	[SerializeField] private List<Image> iconList = new List<Image>();
	private void OnEnable()
	{
		WeaponSwitch.onWeaponSwitch += IconEnable;
	}

	private void OnDisable()
	{
		WeaponSwitch.onWeaponSwitch -= IconEnable;
	}

	public  void IconEnable(int iconIndex)
	{
		for(int i = 0; i < iconList.Count; i++)
		{
			iconList[i].color = Color.grey;
		}
		iconList[iconIndex].color = Color.white;
	}

}
