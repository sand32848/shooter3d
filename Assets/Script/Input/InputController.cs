using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public static InputController _instance;
    public static InputController Instance => _instance;

    public PlayerAction inputActions;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        inputActions = new PlayerAction();
    }
    private void OnEnable()
    {
        inputActions.Enable();
    }
    private void OnDisable()
    {
        inputActions.Disable();
    }

    public Vector2 movement => inputActions.PlayerControl.Movement.ReadValue<Vector2>();
    public Vector2 mouseInput => inputActions.PlayerControl.MouseInput.ReadValue<Vector2>();
    public bool LeftClick => inputActions.PlayerControl.Shoot.triggered;
    public bool LeftClickHold => inputActions.PlayerControl.Shoot.ReadValue<float>() > 0.1f;
    public bool RightClick => inputActions.PlayerControl.Zoom.triggered;
    public bool R => inputActions.PlayerControl.Reload.triggered;
    public bool E => inputActions.PlayerControl.Interact.triggered;
    public bool ESC => inputActions.UI.ESC.triggered;
    public bool Space => inputActions.PlayerControl.Jump.triggered;
    public bool LeftShift => inputActions.PlayerControl.LeftShift.ReadValue<float>() < 0.1f;
    public bool debugHealth => inputActions.PlayerControl.DebugHealth.triggered;
    public bool debugMoney => inputActions.PlayerControl.DebugMoney.triggered;
    public bool Weapon1 => inputActions.PlayerControl.Weapon1.triggered;
    public bool Weapon2 => inputActions.PlayerControl.Weapon2.triggered;
    public bool Weapon3 => inputActions.PlayerControl.Weapon3.triggered;


    //public float RightHold => inputActions.PlayerControl.RightHold.ReadValue<float>();
    //public bool LeftClick => inputActions.PlayerControl.LeftClick.triggered;
    //public bool RightClick => inputActions.PlayerControl.RightClick.triggered;

    //public bool Spacebar => inputActions.PlayerControl.Spacebar.triggered;
    //public bool Console => inputActions.PlayerControl.Console.triggered;

    //public void EnableMouseLook() => inputActions.PlayerControl.MouseLook.Enable();

    //public void DisableMouseLook() => inputActions.PlayerControl.MouseLook.Disable();

    public void enableInput()
    {
        inputActions.PlayerControl.Enable();
    }

    public void disableInput()
    {
        inputActions.PlayerControl.Disable();
    }

    public void disableMovement()
	{
        inputActions.PlayerControl.Movement.Disable();
	}

    public void enableMovement()
    {
        inputActions.PlayerControl.Movement.Enable();
    }
}
