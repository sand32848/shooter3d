using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarManager : MonoBehaviour
{
    [SerializeField] private Image healthBarFill;
	// Start is called before the first frame update

	private void OnEnable()
	{
		PlayerHealth.onHealthChange += UpdateHealthUI;
	}

	private void OnDisable()
	{
		PlayerHealth.onHealthChange -= UpdateHealthUI;
	}

	private void UpdateHealthUI(float newHealth)
	{
		healthBarFill.fillAmount = (newHealth / 100f);
	}
}
