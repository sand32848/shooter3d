using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateWithCooldown : EnemyState
{
	[SerializeField]protected float cooldown = 0;
	protected float _cooldown = 0;

	public virtual void Start()
	{
		_cooldown = cooldown;
	}

	public override void RunCurrentState()
	{

	}

	public virtual void Update()
	{
		cooldownCount();
	}

	public bool isOffCooldown()
	{
		if(cooldown > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public void cooldownRefresh()
	{
		cooldown = _cooldown;
	}

	public void cooldownCount()
	{
		cooldown -= Time.deltaTime;
	}
}
