using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCountUpgrade : GunUpgrade
{
	[SerializeField]	private ShotGun shotGun;
	private void Start()
	{
		shotGun = GetComponent<ShotGun>();
	}
	public override void upgradeTrigger()
	{
		shotGun.shotAmount = (int)applyUpgrade();
	}
}
