using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public State currentState;
    public List<State> stateList = new List<State>();
	private List<StateWithCooldown> stateWithCooldownsList = new List<StateWithCooldown>();
	[SerializeField] private GameObject stateHolder;
	// Update is called once per frame

	private void Start()
	{
		initializeState();
	}

	void Update()
    {
        RunStateMachine();
    }

	private void RunStateMachine()
	{
        currentState?.RunCurrentState();
    }

    private void SwitchToTheNextState(State nextState)
	{
        currentState = nextState;
	}
	public void runState (System.Type stateType)
	{
		SwitchToTheNextState(getState(stateType));
		//print(stateType);
	}

	public void initializeState()
	{
		Component[] stateArray;
		stateArray = stateHolder.GetComponents(typeof(State));

		for(var i = 0; i < stateArray.Length; i++)
		{
			stateList.Add(stateArray[i] as State);
		}

		Component[] stateCooldownArray;
		stateCooldownArray = stateHolder.GetComponents(typeof(StateWithCooldown));

		for (var i = 0; i < stateCooldownArray.Length; i++)
		{
			stateWithCooldownsList.Add(stateCooldownArray[i] as StateWithCooldown);
		}
	}

	public State getState(System.Type stateType)
	{
		State state = null;

		for (var i = 0; i < stateList.Count; i++)
		{
			if (stateList[i].GetType() == stateType)
			{
				state = stateList[i];
				return state;
			}
		}

		Debug.Log(stateType + "does not exist");
		return null;
	}

	public  StateWithCooldown getStateCooldown(System.Type stateType)
	{
		StateWithCooldown state = null;

		for (var i = 0; i < stateList.Count; i++)
		{
			if (stateWithCooldownsList[i].GetType() == stateType)
			{
				state = stateWithCooldownsList[i];
				return state;
			}
		}
		Debug.Log(state + "does not exist/ cooldownVer");
		return null;
	}


	public bool AllOnCooldown()
	{
		for(var i = 0; i < stateWithCooldownsList.Count; i++)
		{
			if (stateWithCooldownsList[i].isOffCooldown())
			{
				return false;
			}
		}
		return true;
	}

}
