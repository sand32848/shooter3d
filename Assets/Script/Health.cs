using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Health : MonoBehaviour
{
	[SerializeField]  private UnityEvent onDeathEvent;
    [SerializeField] private float currentHealth;
	[SerializeField] private float maxHealth;
	private bool isDead = false;


	public void reduceHealth(float _health)
	{
        currentHealth -= _health;

        if(currentHealth <= 0 && !isDead)
		{
			isDead = true;
			Death();
		}
	}

    public void Death()
	{
		onDeathEvent?.Invoke();
		Destroy(gameObject);
	}



	public float getHealthPercent()
	{
		return (currentHealth / maxHealth) * 100;
	}

}
