using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClipSizeUpgrade : GunUpgrade
{
	public override void upgradeTrigger()
	{
		gunStat.MaxAmmo = applyUpgrade();
		gunStat.CallUpdateAmmo();
	}
}
