using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireRateUpgrade : GunUpgrade
{
	public override void upgradeTrigger()
	{
		gunStat.fireRate = applyUpgrade();
	}
}
