using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageUpgrade : GunUpgrade
{
	public override void upgradeTrigger()
	{
		gunStat.firePower = applyUpgrade();
	}
}
