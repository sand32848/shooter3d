using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadUpgrade : GunUpgrade
{
	public override void upgradeTrigger()
	{
		gunStat.reloadTime = applyUpgrade();
	}
}
