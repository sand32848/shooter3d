using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpeedUpgrade : UpgradeAttribute
{
	private PlayerMovement playerMovement => player.GetComponent<PlayerMovement>();
	public override void applyModifier()
	{
		increaseLevel();

		switch (currentLevel)
		{
			case 1:
				playerMovement.ChangeMoveSpeed(playerMovement.speed * 1.1f);
				break;
			case 2:
				playerMovement.ChangeMoveSpeed(playerMovement.speed * 1.1f);
				break;
			case 3:
				playerMovement.ChangeMoveSpeed(playerMovement.speed * 1.1f);
				break;
		}
	}


}
