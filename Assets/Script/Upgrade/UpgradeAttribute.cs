using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UpgradeAttribute : MonoBehaviour
{

    [SerializeField] protected int currentLevel;
    [SerializeField] protected int maxLevel;
    protected GameObject player;

	public virtual void Start()
	{
        player = GameObject.FindWithTag("Player");
	}
	public abstract void applyModifier();

    public virtual void increaseLevel()
	{
        currentLevel += 1;
        currentLevel = Mathf.Clamp(currentLevel,0,maxLevel);
	}

    public int GetLevel()
	{
        return currentLevel;
	}

    public int GetMaxLevel()
	{
        return maxLevel;
	}


}
