using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpgradeManager : MonoBehaviour
{
	[SerializeField] private GameObject upgradePanel;
	[SerializeField] private GameObject upgradeBank;
	[SerializeField] private TextMeshProUGUI moneyText;

	private List<UpgradeAttribute> upgradeList = new List<UpgradeAttribute>();
	private List<UpgradeAttribute> upgradeToRemove = new List<UpgradeAttribute>();
	private List<int> upgradeIndex = new List<int>();

    private void Update()
    {
		moneyText.text = "$:" + MoneyManager.Instance.moneyBank.ToString();
    }

    private void OnEnable()
	{
		WaveManager.onWaveEnd += triggerUpgrade;
	}

	private void OnDisable()
	{
		WaveManager.onWaveEnd -= triggerUpgrade;
	}


	public void triggerUpgrade()
	{
		upgradePanel.SetActive(true);
		PauseManager.pausable = false;
	}

    public void setPauseable()
    {
		PauseManager.pausable = true;
    }


}
