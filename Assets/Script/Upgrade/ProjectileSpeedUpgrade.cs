using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpeedUpgrade : UpgradeAttribute
{
	PlayerGunMultiplier playerShooting => player.GetComponent<PlayerGunMultiplier>();
	public override void applyModifier()
	{
		increaseLevel();

		switch (currentLevel)
		{
			case 1:
				playerShooting.ChangeProjectileSpeed(playerShooting.bulletSpeedMulti * 1.1f);
				break;
			case 2:
				playerShooting.ChangeProjectileSpeed(playerShooting.bulletSpeedMulti * 1.1f);
				break;
			case 3:
				playerShooting.ChangeProjectileSpeed(playerShooting.bulletSpeedMulti * 1.1f);
				break;
		}
	}


}
