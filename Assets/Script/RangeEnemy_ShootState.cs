using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy_ShootState : EnemyState
{
	[SerializeField] private float fireRatePerSec;
	[SerializeField] private GameObject bullet;
	[SerializeField] private float damage;
	[SerializeField] private float bulletSpeed;
	private float _fireRatePerSec;
	[SerializeField] private float outRange;
	private void Start()
	{
		_fireRatePerSec = fireRatePerSec;
	}
	public override void RunCurrentState()
	{
		navMesh.speed = 0;

		if(enemyAI.distanceBetweenPlayer() > outRange)
		{
			stateManager.runState(typeof(RangeEnemy_MoveState));
		}

		fireRatePerSec -= Time.deltaTime;

		if (fireRatePerSec <= 0)
		{
			GameObject _bullet = Instantiate(bullet, transform.position, Quaternion.identity);
			fireRatePerSec = _fireRatePerSec;
			_bullet.GetComponent<Rigidbody>().AddForce((enemyAI.player.transform.position - transform.position).normalized * bulletSpeed, ForceMode.Impulse);
			_bullet.GetComponent<EnemyBullet>().setDamage(damage);
		}
	}
	private void OnDrawGizmosSelected()
	{
		Gizmos.DrawWireSphere(transform.position, outRange);
	}
}
