using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class WaveManager : MonoBehaviour
{
    private int minWaveRequire = 1;
    private int totalRequire = 1;
    private int currentWave = 0;
    private int currentMiniWave = 0;
    [SerializeField] private List<WaveSetting> waveSettings = new List<WaveSetting>();
    private Spawner spawner;
    public static Action onWaveEnd;
    public UnityEvent onFinalWaveCompleted;

	private void OnDisable()
	{
        Enemy.onDeath -= deathCheck;
    }

	private void OnEnable()
	{
        Enemy.onDeath += deathCheck;
    }

	void Start()
    {
        spawner = GetComponent<Spawner>();
        totalRequire = waveSettings[currentWave].ReturnTotalEnemyCount();
        minWaveRequire = waveSettings[currentWave].ReturnWaveEnemyCount(currentMiniWave);
        spawnEnemy();
    }

    public  void spawnEnemy()
	{
        UpdateWaveStat();

        var list = waveSettings[currentWave].MiniWaveList[currentMiniWave].enemySettingList;
        int currentEnemyIndex = 0;

        for (int i = 0; i < list.Count; i++)
        {
            currentEnemyIndex += 1;

            for(int j = 0; j < list[i].enemyCount; j ++)
			{
                spawner.AddToSpawnList(list[i].enemyType);
            }
        }


    }

    public  void deathCheck()
	{
        totalRequire -= 1;
        minWaveRequire -= 1;

        UpdateWaveStat();

    }

    private void UpdateWaveStat()
    {
        // wave end here
        if (totalRequire <= 0)
        {
            //check final wave
            if (currentWave +1 >= waveSettings.Count)
            {
                InputController.Instance.disableInput();
                onFinalWaveCompleted.Invoke();
                return;
            }

            currentWave += 1;
            currentMiniWave = 0;
            totalRequire = waveSettings[currentWave].ReturnTotalEnemyCount();
            minWaveRequire = waveSettings[currentWave].ReturnWaveEnemyCount(currentMiniWave);
            onWaveEnd?.Invoke();
            InputController.Instance.disableInput();
            return;
        }

        // mini wave end here
        if (minWaveRequire <= 0)
        {
            currentMiniWave += 1;
            minWaveRequire = waveSettings[currentWave].ReturnWaveEnemyCount(currentMiniWave);
            spawnEnemy();
            return;
        }

    }
}
