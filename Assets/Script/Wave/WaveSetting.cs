using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wave", menuName = "WaveSetting/Wave", order = 1)]

[System.Serializable]
public class WaveSetting : ScriptableObject
{
    public RewardType rewardType = RewardType.normal;
    public List<MiniWave> MiniWaveList = new List<MiniWave>();

    public int ReturnTotalEnemyCount()
    {
        int total = 0;
        for(int i  = 0; i < MiniWaveList.Count; i++) // 2
		{
            for(int j  = 0; j < MiniWaveList[i].enemySettingList.Count; j++)
			{
                total += MiniWaveList[i].enemySettingList[j].enemyCount;
            }
		}
        return total;
    }

    public  int ReturnWaveEnemyCount(int index)
	{
        int count = 0;
        for(int i = 0; i < MiniWaveList[index].enemySettingList.Count; i ++)
		{
            count += MiniWaveList[index].enemySettingList[i].enemyCount;
        }
        return count;
	}
}

[System.Serializable]
public class EnemySetting
{
    public GameObject enemyType;
    public int enemyCount;
}

[System.Serializable]
public class MiniWave
{
   public List<EnemySetting> enemySettingList = new List<EnemySetting>();
}

public enum RewardType {normal,super}


