using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
	public string songName;
	private void Start()
	{
		AudioManager.instance?.Play(songName);
	}

}
