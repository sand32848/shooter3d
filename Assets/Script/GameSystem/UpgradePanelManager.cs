using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradePanelManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> tabList = new List<GameObject>();

    public void changeTab(int tabIndex)
	{
		for(int i = 0; i < tabList.Count; i++)
		{
			tabList[i].SetActive(false);
		}

		tabList[tabIndex].SetActive(true);
	}
}
