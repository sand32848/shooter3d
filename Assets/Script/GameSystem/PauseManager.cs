using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class PauseManager : MonoBehaviour
{
	[SerializeField] private UnityEvent onPauseEvent;
	[SerializeField] private UnityEvent onUnpauseEvent;
	public static Action pauseSignal;
    private bool paused = false;
	public  static bool pausable = true;
   

    // Update is called once per frame
    void Update()
    {
        if (!pausable)
        {
			return;
        }

		if (InputController.Instance.ESC)
		{
			paused = !paused;

			if (paused)
			{
				Time.timeScale = 0;
				onPauseEvent.Invoke();
				InputController.Instance.disableInput();

			}
			else
			{
				Time.timeScale = 1;
				onUnpauseEvent.Invoke();
				InputController.Instance.enableInput();
			}
		}
    }

	public void PauseSwitch()
	{
		paused = false;
		Time.timeScale = 1;
		onUnpauseEvent.Invoke();
		InputController.Instance.enableInput();
	}
}
