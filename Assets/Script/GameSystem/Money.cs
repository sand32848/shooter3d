using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Money : MonoBehaviour
{
    [SerializeField] private int money;

    public static UnityAction<float> onMoneyDrop;

    public void MoneyDrop()
	{
        onMoneyDrop?.Invoke(money);
	}
}
