using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MoneyUIManager : MonoBehaviour
{
	[SerializeField] private TextMeshProUGUI moneyText;
	private void OnEnable()
	{
		MoneyManager.onMoneyChange += updateMoneyText;
	}

	private void OnDisable()
	{
		MoneyManager.onMoneyChange -= updateMoneyText;
	}
	
	public void updateMoneyText(float _money)
	{
		moneyText.text = "$" + _money.ToString();
	}
}
