using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MoneyManager : MonoBehaviour
{
	public static UnityAction<float> onMoneyChange;
	[field: SerializeField]public  float moneyBank {get;set;} = 0;

	public static MoneyManager _instance;
	public static MoneyManager Instance => _instance;

    private void Awake()
    {
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			_instance = this;
		}
	}

    private void Update()
    {
        if (InputController.Instance.debugMoney)
        {
			changeMoney(50000);
        }
    }

    private void OnEnable()
	{
		Money.onMoneyDrop += changeMoney;
		UpgradeButtonUI.onUpgradePress += changeMoney;
	}

	private void OnDisable()
	{
		Money.onMoneyDrop -= changeMoney;
		UpgradeButtonUI.onUpgradePress -= changeMoney;
	}

	public void changeMoney(float _money)
	{
		moneyBank += _money;
		onMoneyChange?.Invoke(moneyBank);
	}

}
