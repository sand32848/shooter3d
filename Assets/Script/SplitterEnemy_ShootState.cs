using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitterEnemy_ShootState : EnemyState
{
	[SerializeField] private float fireRatePerSec;
	[SerializeField] private GameObject bullet;
	[SerializeField] private float damage;
	[SerializeField] private float bulletSpeed;
	[SerializeField] private GameObject shootFolder;
	private float _fireRatePerSec;
	[SerializeField] private float outRange;
	private void Start()
	{
		_fireRatePerSec = fireRatePerSec;
	}
	public override void RunCurrentState()
    {
		navMesh.speed = 0;

		if (enemyAI.distanceBetweenPlayer() > outRange)
		{
			stateManager.runState(typeof(SpliiterEnemy_MoveState));
		}

		fireRatePerSec -= Time.deltaTime;


		if (fireRatePerSec <= 0)
		{
			for (int i = 0; i < shootFolder.transform.childCount; i++)
			{
				GameObject _bullet = Instantiate(bullet, transform.position, Quaternion.identity);
				fireRatePerSec = _fireRatePerSec;
				_bullet.GetComponent<Rigidbody>().AddForce(shootFolder.transform.GetChild(i).transform.forward * bulletSpeed, ForceMode.Impulse);
				_bullet.GetComponent<EnemyBullet>().setDamage(damage);
			}

		}
	}

}
