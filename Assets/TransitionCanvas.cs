using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using DG.Tweening;

public class TransitionCanvas : MonoBehaviour
{
    public static TransitionCanvas instance;
    [SerializeField] private CanvasGroup canvasGroup;
    [SerializeField] private GameObject blackScreen;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

    }

    public void callTrans(string sceneName)
    {
        blackScreen.SetActive(true);
        DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, 1f, 0.5f).OnComplete(() => exitTrans(sceneName));
    }

    public void exitTrans(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
        DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, 0f, 0.5f).OnComplete(() =>  DisableBlack());
    }

    public void DisableBlack()
    {
        blackScreen.SetActive(false);
    }
}
