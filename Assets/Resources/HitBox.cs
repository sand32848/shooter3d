using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HitBox : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private UnityEvent<float> onHitFloat;
    [SerializeField] private UnityEvent onHit;

    public void hit(float damage)
	{
        onHit.Invoke();
        onHitFloat.Invoke(damage);
	}
}
